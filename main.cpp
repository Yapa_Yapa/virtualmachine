#include <iostream>
#include "Types/Tryte/Tryte.h"
#include "Types/Int/Int.h"
#include "Types/Float/Float.h"

#include "Memory/Memory.h"
#include "Proccessor/Proccessor.h"

using namespace std;

int main()
{
    std::string code = "reg\n"
                       "mov RA 222\n"
                       "mov RB 2\n"
                       "add RA RB\n"
                       "reg";

    Memory memory;
    memory.load(code);

    Proccessor proccessor(memory);
    proccessor.start();

    return 0;
}

