TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Types/Tryte/Tryte.cpp \
    Types/Int/Int.cpp \
    Types/Float/Float.cpp \
    Proccessor/Proccessor.cpp \
    Memory/Memory.cpp

HEADERS += \
    Types/Types.h \
    Types/Tryte/Tryte.h \
    Types/Int/Int.h \
    Types/Float/Float.h \
    Proccessor/Proccessor.h \
    Memory/Memory.h

