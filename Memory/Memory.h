#ifndef MEMORY_H
#define MEMORY_H

#include <vector>
#include <stack>

#include "../Types/Int/Int.h"

class Memory
{
private:
    std::vector<std::string> _code;
    std::stack<Int> _data;
public:
    Memory(){}

    void load(const std::string& sourceCode)
    {
        _code = split(sourceCode, "\n");
    }

    std::vector<std::string> command(Int pos)
    {
        unsigned position = int(pos);

        if(position >= _code.size())
            return std::vector<std::string>();

        std::vector<std::string> result = split(_code.at(position), " ");

        if(result.size() > 3)
            std::invalid_argument("Command is not supported "
                                  "[" + _code.at(position) + "]");

        while(result.size() < 3)
        {
            result.push_back("");
        }

        return result;
    }

    void push_to_stack(Int a)
    {
        _data.push(a);
    }

    Int pop_from_stack()
    {
        Int top = _data.top();
        _data.pop();

        return top;
    }

private:
    std::vector<std::string> split(std::string a, std::string b)
    {
        std::vector<std::string> r;
        int i = 0;
        while (i != -1) {
            i = a.find(b);

            r.push_back(a.substr(0,i));
            a = a.substr(i+b.size(),a.size());
        }
        return r;
    }


};

#endif // MEMORY_H
