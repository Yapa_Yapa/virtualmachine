#include "Float.h"

Float::Float(const std::string &value)
{
    if( value.size() > COUNT_TRITS*SIZE_FLOAT )
        throw std::bad_alloc();

    for(unsigned i = 0; i < value.size(); i++)
        if( (value.at(i) < '0' || value.at(i) > '2') && value.at(i) != '.' )
            throw std::bad_alloc();

    this->_value = Tryte::make("0", SIZE_FLOAT);

    std::string v = "";

    unsigned i = 0;
    for(; i < value.size() &&
          i < COUNT_TRITS*SIZE_FLOAT_INT &&
          value.at(i) != '.';
        i++)
    {
        v += value.at(i);
    }

    while(v.size() < COUNT_TRITS*SIZE_FLOAT_INT)
    {
        v = "0" + v;
    }

    for(; i < value.size() && i < COUNT_TRITS*SIZE_FLOAT_INT; i++)
    {
        if( value.at(i)  == '.' )
            continue;

        v += value.at(i);
    }
    while(v.size() < COUNT_TRITS*SIZE_FLOAT_INT)
    {
        v += "0";
    }

    for(unsigned i = 0; i < v.size(); i++)
    {
        this->setTrit(i, v.at(i) - '0');
    }
}

Float::~Float()
{
    /*delete [] this->_value;

    this->_value = nullptr;*/
}

Float Float::operator+(const Float &b)
{
    Float newFloat;

    newFloat._value = Tryte::add(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);

    return newFloat;
}


Float Float::operator*(const Float &b)
{
    Float newFloat;

    Float temp;

    temp._value = Tryte::mul(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);

    // Смещение положение . на 6 вправо с потерей младших трит
    for(unsigned i = 0; i < COUNT_TRITS; i++)
    {
        newFloat.setTrit(i, 0);
    }
    for(unsigned i = 0; i < COUNT_TRITS*SIZE_FLOAT_INT; i++)
    {
        newFloat.setTrit(i+COUNT_TRITS, temp.trit(i));
    }

    return newFloat;
}

Float Float::operator-(const Float &b)
{
    Float newFloat;

    newFloat._value = Tryte::sub(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);

    return newFloat;
}

Float Float::operator/(const Float &b)
{
    Float newFloat;

    newFloat._value = Tryte::div(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);

    return newFloat;
}

bool Float::operator==(const Float &b)
{
    return  Tryte::eq(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);
}

bool Float::operator!=(const Float &b)
{
    return  Tryte::ne(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);
}

bool Float::operator<(const Float &b)
{
    return  Tryte::lt(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);
}

bool Float::operator>(const Float &b)
{
    return  Tryte::gt(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);
}

bool Float::operator>=(const Float &b)
{
    return  Tryte::ge(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);
}

bool Float::operator<=(const Float &b)
{
    return  Tryte::le(this->_value, SIZE_FLOAT, b._value, SIZE_FLOAT);
}

unsigned char Float::trit(unsigned i) const
{
    return Tryte::tritFromArray(this->_value, i, SIZE_FLOAT);
}

void Float::setTrit(unsigned char pos, unsigned char value)
{
    Tryte::setTritFromArray(this->_value, pos, value, SIZE_FLOAT);
}
