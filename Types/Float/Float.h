#ifndef FLOAT_H
#define FLOAT_H

const int SIZE_FLOAT = 5;
const int SIZE_FLOAT_INT = 4;
const int SIZE_FLOAT_FRACTION = 1;

#include <cmath>

#include "../Tryte/Tryte.h"


class Float
{
public:
    Float(){ this->_value = Tryte::make("0", SIZE_FLOAT); }
    Float( const std::string& value );
    ~Float();

    Float                              operator+       (const Float& b);
    Float                              operator*       (const Float& b);
    Float                              operator-       (const Float& b);
    Float                              operator/       (const Float& b);

    bool                                    operator==       (const Float& b);
    bool                                    operator!=       (const Float& b);
    bool                                    operator<        (const Float& b);
    bool                                    operator>        (const Float& b);
    bool                                    operator>=       (const Float& b);
    bool                                    operator<=       (const Float& b);

private:
    unsigned char                           trit            (unsigned i)          const;
    void                                    setTrit         (unsigned char pos,
                                                             unsigned char value);

protected:
    friend std::ostream& operator<<(std::ostream& os, const Float& b)
    {
        bool prefix = true;
        for(int i = 0; i < 30; i++)
        {
            if( prefix && int(b.trit(i)) != 0  )
                prefix = false;

            if( i == 24 )
                os << '.';

            if( !(int(b.trit(i)) == 0 && prefix) )
                os << int(b.trit(i));
        }

        return os;
    }

protected:
    pTryte _value;
};

#endif // FLOAT_H
