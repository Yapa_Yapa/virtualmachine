#include "Int.h"

Int::Int(const std::string &value)
{
    if( value.size() > COUNT_TRITS*SIZE_INT )
        throw std::bad_alloc();

    for(unsigned i = 0; i < value.size(); i++)
        if( value.at(i) < '0' || value.at(i) > '2'  )
            throw std::bad_alloc();

    this->_value = Tryte::make("0", SIZE_INT);

    std::string v = value;
    while( v.size() < COUNT_TRITS*SIZE_INT)
    {
        v = "0" + v;
    }

    for(unsigned i = 0; i < v.size(); i++)
    {
        this->setTrit(i, v.at(i) - '0');
    }
}

Int::~Int()
{
    /*delete this->_value;

    this->_value = nullptr;*/
}

Int Int::operator+(const Int &b)
{
    Int newInt;

    newInt._value = Tryte::add(this->_value, SIZE_INT, b._value, SIZE_INT);

    return newInt;
}

Int Int::operator*(const Int &b)
{
    Int newInt;

    newInt._value = Tryte::mul(this->_value, SIZE_INT, b._value, SIZE_INT);

    return newInt;
}

Int Int::operator-(const Int &b)
{
    Int newInt;

    newInt._value = Tryte::sub(this->_value, SIZE_INT, b._value, SIZE_INT);

    return newInt;
}

Int Int::operator/(const Int &b)
{
    Int newInt;

    newInt._value = Tryte::div(this->_value, SIZE_INT, b._value, SIZE_INT);

    return newInt;
}

bool Int::operator==(const Int &b)
{
    return  Tryte::eq(this->_value, SIZE_INT, b._value, SIZE_INT);
}

bool Int::operator!=(const Int &b)
{
    return  Tryte::ne(this->_value, SIZE_INT, b._value, SIZE_INT);
}

bool Int::operator<(const Int &b)
{
    return  Tryte::lt(this->_value, SIZE_INT, b._value, SIZE_INT);
}

bool Int::operator>(const Int &b)
{
    return  Tryte::le(this->_value, SIZE_INT, b._value, SIZE_INT);
}

bool Int::operator>=(const Int &b)
{
    return  Tryte::gt(this->_value, SIZE_INT, b._value, SIZE_INT);
}

bool Int::operator<=(const Int &b)
{
    return  Tryte::ge(this->_value, SIZE_INT, b._value, SIZE_INT);
}

Int::operator int()
{
    int result = 0;
    for(int i = SIZE_INT*COUNT_TRITS-1; i >= 0; i-- )
    {
        int t = this->trit(i);
        result += t * pow(double(3), (SIZE_INT*COUNT_TRITS-1)-i);
    }

    return result;
}

unsigned char Int::trit(unsigned i) const
{
    return Tryte::tritFromArray(this->_value, i, SIZE_INT);
}

void Int::setTrit(unsigned char pos, unsigned char value)
{
    Tryte::setTritFromArray(this->_value, pos, value, SIZE_INT);
}
