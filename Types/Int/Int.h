#ifndef INT_H
#define INT_H

#include <cmath>

#include "../Tryte/Tryte.h"

const int SIZE_INT = 4;

class Int
{
public:
    Int(){ this->_value = Tryte::make("0", SIZE_INT); }
    Int( const std::string& value );
    ~Int();

    Int                                operator+       (const Int& b);
    Int                                operator*       (const Int& b);
    Int                                operator-       (const Int& b);
    Int                                operator/       (const Int& b);

    bool                                    operator==       (const Int& b);
    bool                                    operator!=       (const Int& b);
    bool                                    operator<        (const Int& b);
    bool                                    operator>        (const Int& b);
    bool                                    operator>=       (const Int& b);
    bool                                    operator<=       (const Int& b);\

                                            operator      int();

private:
    unsigned char                           trit            (unsigned i)          const;
    void                                    setTrit         (unsigned char pos,
                                                             unsigned char value);

protected:
    friend std::ostream& operator<<(std::ostream& os, const Int& b)
    {
        bool prefix = true;
        for(int i = 0; i < 24; i++)
        {
            if( prefix && int(b.trit(i)) != 0  )
                prefix = false;

            if( !(int(b.trit(i)) == 0 && prefix) )
                os << int(b.trit(i));
        }

        return os;
    }    
protected:
    pTryte _value;
};

#endif // INT_H
