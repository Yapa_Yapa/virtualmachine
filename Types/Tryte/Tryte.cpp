#include "Tryte.h"

Tryte::Tryte()
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        this->setTrit(i, 0);
    }
}

Tryte::Tryte(const std::string &value)
{
    if( value.size() > COUNT_TRITS )
        throw std::bad_alloc();

    for(unsigned i = 0; i < value.size(); i++)
        if( value.at(i) < '0' || value.at(i) > '2'  )
            throw std::bad_alloc();

    std::string v = value;
    while( v.size() < COUNT_TRITS)
    {
        v = "0" + v;
    }

    for(unsigned i = 0; i < v.size(); i++)
    {
        this->setTrit(i, v.at(i) - '0');
    }
}

Tryte::~Tryte()
{
    /*for(int i = 0; i < COUNT_TRITS; i++)
        setTrit(i, 0);*/
}

unsigned char Tryte::trit(unsigned char pos) const
{
    if( pos > COUNT_TRITS )
        throw std::invalid_argument("pos > 5");

    if(pos == 0)        return t0;
    else if(pos == 1)   return t1;
    else if(pos == 2)   return t2;
    else if(pos == 3)   return t3;
    else if(pos == 4)   return t4;
    else if(pos == 5)   return t5;

    return 0;
}

void Tryte::setTrit(unsigned char pos, unsigned char value)
{
    if( pos > COUNT_TRITS )
        throw std::invalid_argument("pos > 5");
    if( value > 2 )
        throw std::invalid_argument("value > 2");

    if(pos == 0)        t0 = value;
    else if(pos == 1)   t1 = value;
    else if(pos == 2)   t2 = value;
    else if(pos == 3)   t3 = value;
    else if(pos == 4)   t4 = value;
    else if(pos == 5)   t5 = value;
}

Tryte Tryte::operator+(const Tryte &b) const
{
    Tryte newTryte;
    unsigned char overTrit = 0;

    for(int i = COUNT_TRITS-1; i >= 0; i--)
    {
        unsigned char newTrit = (this->trit(i) + b.trit(i) + overTrit) % 3;
        newTryte.setTrit(i, newTrit);
        overTrit = (this->trit(i) + b.trit(i) + overTrit) / 3;
    }

    return newTryte;
}

Tryte Tryte::operator*(const Tryte &b) const
{
    Tryte newTryte;

    Tryte counter = b;
    while( counter > Tryte("0") )
    {
        newTryte = newTryte + *this;
        counter = counter - Tryte("1");
    }

    return newTryte;
}

Tryte Tryte::operator-(const Tryte &b) const
{
    Tryte newTryte;

    unsigned char overTrit = 0;
    for(int i = COUNT_TRITS-1; i >= 0; i--)
    {
        unsigned char newTrit = 0;
        if( (this->trit(i) - overTrit) < b.trit(i) )
        {
            newTrit = (this->trit(i) - overTrit + 3) - b.trit(i);
            overTrit = 1;
        }
        else
        {
            newTrit = this->trit(i) - b.trit(i) - overTrit;
            overTrit = 0;
        }

        newTryte.setTrit(i, newTrit);
    }

    return newTryte;
}

Tryte Tryte::operator/(const Tryte &b) const
{
    if( (*this) < b )
        return Tryte("0");

    Tryte newTryte;

    Tryte temp = *this;
    while( temp >= b )
    {
        temp = temp - b;
        newTryte = newTryte + Tryte("1");
    }

    return newTryte;
}

bool Tryte::operator==(const Tryte &b) const
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        if( this->trit(i) != b.trit(i) )
            return false;
    }

    return true;
}

bool Tryte::operator!=(const Tryte &b) const
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        if( this->trit(i) != b.trit(i) )
            return true;
    }

    return false;
}

bool Tryte::operator<(const Tryte &b) const
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        if( this->trit(i) < b.trit(i) )
            return true;
        else if( this->trit(i) > b.trit(i) )
            return false;
    }

    return false;
}

bool Tryte::operator<=(const Tryte &b) const
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        if( this->trit(i) < b.trit(i) )
            return true;
        else if( this->trit(i) > b.trit(i) )
            return false;
    }

    return true;
}

bool Tryte::operator>(const Tryte &b) const
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        if( this->trit(i) > b.trit(i) )
            return true;
        else if( this->trit(i) < b.trit(i) )
            return false;
    }

    return false;
}

bool Tryte::operator>=(const Tryte &b) const
{
    for(int i = 0; i < COUNT_TRITS; i++)
    {
        if( this->trit(i) > b.trit(i) )
            return true;
        else if( this->trit(i) < b.trit(i) )
            return false;
    }

    return true;
}




unsigned char Tryte::tritFromArray(const pTryte a, unsigned pos, unsigned sizeA)
{
    if(a == NULL)
        throw std::invalid_argument("Tryte::tritFromArray - first operand are NULL");
    if(pos >= COUNT_TRITS*sizeA)
        throw std::invalid_argument("Tryte::tritFromArray - pos out of range");


    if( pos > COUNT_TRITS-1 )
        return a.get()[pos / (COUNT_TRITS-1)].trit(pos % (COUNT_TRITS-1));
    else
        return a.get()[0].trit(pos);
}

void Tryte::setTritFromArray(pTryte a, unsigned pos, unsigned char value, unsigned sizeA)
{
    if(a == NULL)
        throw std::invalid_argument("Tryte::setTritFromArray - first operand are NULL");
    if(pos >= COUNT_TRITS*sizeA)
        throw std::invalid_argument("Tryte::setTritFromArray - pos out of range");

    if( pos > COUNT_TRITS-1 )
        a.get()[pos / (COUNT_TRITS-1)].setTrit(pos % (COUNT_TRITS-1), value);
    else
        a.get()[0].setTrit(pos, value);
}

pTryte Tryte::copy(const pTryte a, unsigned sizeA)
{
    if(a == NULL)
        throw std::invalid_argument("Tryte::copy - first operand are NULL");

    pTryte newTryteArray = make("0", sizeA);

    for(unsigned i = 0; i < COUNT_TRITS*sizeA; i++)
        setTritFromArray(newTryteArray, i, tritFromArray(a, i, sizeA), sizeA);

    return newTryteArray;
}

pTryte Tryte::make(const std::string &value, unsigned size)
{
    for(unsigned i = 0; i < value.size(); i++)
        if( value.at(i) < '0' || value.at(i) > '2'  )
            throw std::bad_alloc();

    pTryte type(new Tryte[size]);

    std::string v = value;
    while( v.size() < size*COUNT_TRITS)
    {
        v = "0" + v;
    }

    for(unsigned i = 0; i < v.size(); i++)
    {
        setTritFromArray(type, i, v.at(i) - '0', size);
    }

    return type;
}

pTryte Tryte::add(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::add - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::add - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::add - second operand are NULL");

    pTryte newTryte = make("0", sizeA);

    unsigned char newTrit = 0; //Экономия на времени под выделение памяти
    unsigned char overTrit = 0;
    for(int i = COUNT_TRITS * sizeA -1; i >= 0; i--)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        newTrit = (left + right + overTrit) % 3;
        overTrit = (left + right + overTrit) / 3;

        setTritFromArray(newTryte, i, newTrit, sizeA);
    }

    return newTryte;
}

pTryte Tryte::mul(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::mul - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::mul - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::mul - second operand are NULL");

    pTryte newTryte = make("0", sizeA);

    pTryte counter = copy(b, sizeA);
    pTryte zero = make("0", sizeA); // Определили констатнту 0
    pTryte one = make("1", sizeA); // Определили константу 1

    while( gt(counter, sizeA, zero, sizeA) )
    {
        newTryte = add(newTryte, sizeA, a, sizeA);
        counter = sub(counter, sizeA, one, sizeA);
    }

    return newTryte;
}

pTryte Tryte::sub(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::sub - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::sub - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::sub - second operand are NULL");

    pTryte newTryte = make("0", sizeA);

    unsigned char overTrit = 0;
    for(int i = COUNT_TRITS * sizeA - 1; i >= 0; i--)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);
        unsigned char newTrit = 0;

        if( (left - overTrit) < right )
        {
            newTrit = (left - overTrit + 3) - right;
            overTrit = 1;
        }
        else
        {
            newTrit = left - right - overTrit;
            overTrit = 0;
        }

        setTritFromArray(newTryte, i, newTrit, sizeA);
    }

    return newTryte;
}




pTryte Tryte::div(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::add - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::add - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::add - second operand are NULL");

    if( lt(a, sizeA, b, sizeA) )
        return make("0", sizeA);

    pTryte newTryte = make("0", sizeA);

    pTryte one = make("1", sizeA); // Определили константу 1
    pTryte temp = copy(a, sizeA);

    while( ge(temp, sizeA, b, sizeA) )
    {
        temp = sub(temp, sizeA, b, sizeA);
        newTryte = add(newTryte, sizeA, one, sizeA);
    }

    return newTryte;
}

bool Tryte::eq(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::eq - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::eq - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::eq - second operand are NULL");

    for(unsigned i = 0; i < COUNT_TRITS * sizeA; i++)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        if( left != right )
            return false;
    }

    return true;
}

bool Tryte::ne(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::ne - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::ne - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::ne - second operand are NULL");

    for(unsigned i = 0; i < COUNT_TRITS * sizeA; i++)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        if( left != right )
            return true;
    }

    return false;
}

bool Tryte::lt(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::lt - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::lt - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::lt - second operand are NULL");

    for(unsigned i = 0; i < COUNT_TRITS * sizeA; i++)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        if( left < right )
            return true;
        else if( left > right )
            return false;
    }

    return false;
}

bool Tryte::le(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::le - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::le - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::le - second operand are NULL");

    for(unsigned i = 0; i < COUNT_TRITS * sizeA; i++)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        if( left < right )
            return true;
        else if( left > right )
            return false;
    }

    return true;
}

bool Tryte::gt(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::gt - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::gt - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::gt - second operand are NULL");

    for(unsigned i = 0; i < COUNT_TRITS * sizeA; i++)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        if( left > right )
            return true;
        else if( left < right )
            return false;
    }

    return false;
}

bool Tryte::ge(const pTryte a, unsigned sizeA, const pTryte b, unsigned sizeB)
{
    if(sizeA != sizeB)
        throw std::invalid_argument("Tryte::ge - size of operands are not equal");
    if(a == NULL)
        throw std::invalid_argument("Tryte::ge - first operand are NULL");
    if(b == NULL)
        throw std::invalid_argument("Tryte::ge - second operand are NULL");

    for(unsigned i = 0; i < COUNT_TRITS * sizeA; i++)
    {
        unsigned char left = tritFromArray(a, i, sizeA);
        unsigned char right = tritFromArray(b, i, sizeA);

        if( left > right )
            return true;
        else if( left < right )
            return false;
    }

    return true;
}


