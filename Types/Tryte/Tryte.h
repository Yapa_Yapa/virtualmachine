#ifndef TRYTE_H
#define TRYTE_H

const int SIZE_TRYTE = 1;
const int COUNT_TRITS = 6;

#include <iostream>
#include <stdexcept>
#include <string>
#include <memory>

class Tryte;

typedef std::shared_ptr<Tryte> pTryte;

class Tryte
{
public:
    Tryte();
    Tryte(const std::string &value );
    ~Tryte();

    unsigned char                               trit                ( unsigned char pos )       const;
    void                                        setTrit             ( unsigned char pos,
                                                                      unsigned char value );

    Tryte                                  operator+           (const Tryte& b)       const;
    Tryte                                  operator*           (const Tryte& b)       const;
    Tryte                                  operator-           (const Tryte& b)       const;
    Tryte                                  operator/           (const Tryte& b)       const;

    bool                                        operator==          (const Tryte& b)       const;
    bool                                        operator!=          (const Tryte& b)       const;
    bool                                        operator<           (const Tryte& b)       const;
    bool                                        operator<=          (const Tryte& b)       const;
    bool                                        operator>           (const Tryte& b)       const;
    bool                                        operator>=          (const Tryte& b)       const;



    static pTryte                               add                 (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB);
    static pTryte                               mul                 (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB);
    static pTryte                               sub                 (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB);
    static pTryte                               div                 (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB);

    static bool                                 eq                  (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB); // ==

    static bool                                 ne                  (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB); // !=

    static bool                                 lt                  (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB); // <

    static bool                                 le                  (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB); // <=

    static bool                                 gt                  (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB); // >

    static bool                                 ge                  (const pTryte a,
                                                                     unsigned sizeA,
                                                                     const pTryte b,
                                                                     unsigned sizeB); // >=

    static unsigned char                        tritFromArray       (const pTryte a,
                                                                     unsigned pos,
                                                                     unsigned sizeA = 1);

    static void                                 setTritFromArray    (pTryte a,
                                                                     unsigned pos,
                                                                     unsigned char value,
                                                                     unsigned sizeA = 1);

    static pTryte                               copy                (const pTryte a,
                                                                     unsigned sizeA);

    static pTryte                               make                (const std::string& value,
                                                                     unsigned size);

public:

    friend std::ostream& operator<<(std::ostream& os, const Tryte& b)
    {
        os << (short)b.t0;
        os << (short)b.t1;
        os << (short)b.t2;
        os << (short)b.t3;
        os << (short)b.t4;
        os << (short)b.t5;

        return os;
    }

private:
    unsigned char t0:2;
    unsigned char t1:2;
    unsigned char t2:2;
    unsigned char t3:2;
    unsigned char t4:2;
    unsigned char t5:2;
};

#endif // TRYTE_H
