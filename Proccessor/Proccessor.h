#ifndef PROCCESSOR_H
#define PROCCESSOR_H

#include <map>

#include "../Types/Int/Int.h"
#include "../Memory/Memory.h"

class Proccessor
{
public:
    Proccessor(Memory& m):memory(m)
    {
        RA = Int("0");
        RB = Int("0");
        RC = Int("0");
        CC = Int("0");

        commands["mov"] = &Proccessor::mov;
        commands["add"] = &Proccessor::add;
        commands["sub"] = &Proccessor::sub;
        commands["mul"] = &Proccessor::mul;
        commands["div"] = &Proccessor::div;
        commands["reg"] = &Proccessor::reg;
    }

    void start()
    {


        std::vector<std::string> cmd = memory.command(CC);
        while( cmd.size() )
        {
            std::cout << "eval: " << cmd.at(0) << " " << cmd.at(1) << " " << cmd.at(2) << std::endl;

            if(commands.find(cmd.at(0)) != commands.end())
            {
                auto func = commands[cmd.at(0)];
                (this->*func)(cmd.at(1), cmd.at(2));
            }

            CC = CC + Int("1");
            cmd = memory.command(CC);
        }
    }

private:
    void mov(const std::string& to, const std::string& from)
    {
        Int value = getValue(from);

        if(to == "RA")
            RA = value;
        else if(to == "RB")
            RB = value;
        else if(to == "RC")
            RC = value;
        else if(to == "CC")
            CC = value;
    }

    void add(const std::string& left, const std::string& right)
    {
        Int a = getValue(left);
        Int b = getValue(right);

        RC = a + b;
    }

    void mul(const std::string& left, const std::string& right)
    {
        Int a = getValue(left);
        Int b = getValue(right);

        RC = a * b;
    }

    void sub(const std::string& left, const std::string& right)
    {
        Int a = getValue(left);
        Int b = getValue(right);

        RC = a - b;
    }

    void div(const std::string& left, const std::string& right)
    {
        Int a = getValue(left);
        Int b = getValue(right);

        RC = a / b;
    }

    void reg(const std::string&, const std::string&)
    {
        std::cout << "RA " << RA << std::endl;
        std::cout << "RB " << RB << std::endl;
        std::cout << "RC " << RC << std::endl;
        std::cout << "CC " << CC << std::endl;
    }

private:
    Int getValue(const std::string& v) const
    {
        Int value("0");

        if(v == "RA")
            value = RA;
        else if(v == "RB")
            value = RB;
        else if(v == "RC")
            value = RC;
        else if(v == "CC")
            value = CC;
        else
            value = Int(v);

        return value;
    }



private:
    Int RA;
    Int RB;
    Int RC;

    Int CC;

    //RAM
    Memory& memory;

    //Commands
    std::map<std::string, void(Proccessor::*)(const std::string&, const std::string&)> commands;
    typedef std::vector<std::string, void(Proccessor::*)(const std::string&, const std::string&)> Commands;
};

#endif // PROCCESSOR_H
